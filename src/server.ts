import { app } from "./app";
import * as http from 'http';
import { Server } from "socket.io";
import { ServerToClientEvents, ClientToServerEvents, 
    InterServerEvents, SocketData } from "./sio/interfaces/interfaces";
import { ValidatePurchase } from "./sio/socketEmitHandlers/socketEmitHandlers";

//server set up
const server = http.createServer(app);
let port = 3000 || process.env.PORT;

//socket set up
const io = new Server
<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>(server);

startServer();
function startServer(){
    io.on('connection', (socket) => {
        socket.on('validateReceipt', (data) => {
            ValidatePurchase(data);
        }),
        io.emit("noArg");
        console.log(socket.id)})

    server.listen(port, () => {console.log('listening on port 3000!')})
}
