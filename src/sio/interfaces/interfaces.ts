import { ReceiptValidationInfo } from "../socketEmitHandlers/socketEmitHandlers";

export{
    ServerToClientEvents,
    ClientToServerEvents,
    InterServerEvents,
    SocketData
}

interface ServerToClientEvents {
    noArg: () => void;
    basicEmit: (a: number, b: string, c: Buffer) => void;
    withAck: (d: string, callback: (e: number) => void) => void;
}

interface ClientToServerEvents {
hello: () => void;
validateReceipt: (data: ReceiptValidationInfo) => void;
}

interface InterServerEvents {
ping: () => void;
}

interface SocketData {
name: string;
age: number;
}